#include <stdio.h>
#include <stdbool.h>
#include "sys/alt_alarm.h"
#include "system.h"
#include "io.h"
#include "altera_up_avalon_ps2.h"
#include "altera_up_ps2_keyboard.h"
#include "altera_avalon_pio_regs.h"
#include "sys/alt_irq.h"
#include "alt_types.h"//alt_u32 is a kind of alt_types
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "VGA.h"
#include "FreeRTOS/timers.h"
#include "convertToBinary.h"
#define Timer_Reset_Task_P      (tskIDLE_PRIORITY+1)



typedef enum {stable, underFrequency, ROC} systemState; //enum for system state
systemState stateVariable = stable;

TimerHandle_t timer;



#define mainREG_TEST_1_PARAMETER    ( ( void * ) 0x12345678 )
#define mainREG_TEST_2_PARAMETER    ( ( void * ) 0x87654321 )
#define mainREG_TEST_PRIORITY       ( tskIDLE_PRIORITY + 1)
#define TASK_STACKSIZE       2048
#define GETSEM_TASK1_PRIORITY             13
#define GET_PS2_ISR_Handler_PRIORITY      14
#define GET_BUTTON_ISR_HANDLER_PRIORITY   14
#define StartInterrupt_Task_PRIORITY 10
#define taskSwitchesPolling_PRIORITY 14
#define taskMonitoring_PRIORITY		(tskIDLE_PRIORITY)+5

SemaphoreHandle_t shared_resource_sem;
SemaphoreHandle_t signal_from_button_isr;
SemaphoreHandle_t signal_from_Timer_ISR;
SemaphoreHandle_t signal_refresh_screen;
//include headers

//Declare Queue
xQueueHandle Global_Queue_Handle = 0;
xQueueHandle Q_New_Frequency = 0;
xQueueHandle Queue_Task_Monitoring = 0;
xQueueHandle Q_freq_data_Monitor_Task =0;
static bool flag = false; //static keeps it the same throughout the files
static bool frequencyReadyToBeSend = false;
static bool maintenanceModeOn = false;
int counter = 0;

static int tens = 0x0;
static int ones = 0x0;

/*loads conditions active = true , disabled = false*/


static Loads activeLoads;

static char inputBuffer[8] = {0};
int indexCount = 0;
TimerHandle_t timer;
TimerHandle_t screenRefreshRate;
TaskHandle_t Timer_Reset;

void ps2_ISR_Handler(void *pvParameters);
void Button_Interrupt_ISR_Handler(void *pvParameters);
void PRVGADraw_Task(void *pvParameters );
void Start_Interrupts(void *pvParameters);
void Timer_Reset_Task(void *pvParameters);
void vTimerCallback(xTimerHandle t_timer);
void vTimerCallbackVGA(xTimerHandle t_timer);
void taskMonitoring(void *pvParameters);
static void prvFirstRegTestTask(void *pvParameters);

void taskSwitchesPolling(void *pvParameters);


void button_interrupt(void* context, alt_u32 id);
typedef struct{
	unsigned int x1;
	unsigned int y1;
	unsigned int x2;
	unsigned int y2;
}Line;
double freq[100], dfreq[100];
int i = 99, j = 0;
Line line_freq, line_roc;



void taskMonitoring(void *pvParameters){
	while(1){
	double frequency = 0;
	xQueueReceive(Q_freq_data_Monitor_Task,&frequency,1); //wait for 1 tick

	printf("Frequency currently is %d\n",frequency);
	vTaskDelay(20);
	}
}



void ps2_ISR_Handler(void *pvParameters){
	while(1){
		if(xSemaphoreTake(shared_resource_sem,portMAX_DELAY)){
			//needed variables
			//key, ascii, status
			//status is received using the decode_scancode
			//decode_Scancode provides the required information
		char ascii;
		int status = 0;
		unsigned char key = 0;
		KB_CODE_TYPE decode_mode;
		xQueueReceive(Global_Queue_Handle,&status,NULL);
		xQueueReceive(Global_Queue_Handle,&ascii,NULL);
		xQueueReceive(Global_Queue_Handle,&key,NULL);
		xQueueReceive(Global_Queue_Handle,&decode_mode,NULL);

		xQueueReset( Global_Queue_Handle );
		if(status == 0 ){
			if (!flag) //success
			{
				flag = true;
				// print out the result
					switch ( decode_mode )
						{
						case KB_ASCII_MAKE_CODE :
							frequencyReadyToBeSend = false; //reset the flag so the new values can be entered
							printf ( "ASCII   : %c \n", ascii ) ;
							xQueueSend(Q_New_Frequency,&ascii,NULL);
							inputBuffer[indexCount] = ascii;
							indexCount++;
							frequencyReadyToBeSend = false;
							break ;
						case KB_LONG_BINARY_MAKE_CODE :
						// do nothing
						case KB_BINARY_MAKE_CODE :
							if(ascii = 13){
								printf("Enter Key Pressed\n");
									frequencyReadyToBeSend = true;
							}else{
								printf ( "MAKE CODE : %x\n", key ) ;
							}
							break ;
						case KB_BREAK_CODE :
						// do nothing
						default :
							printf ( "DEFAULT   : %x\n", key ) ;
							break ;
					}
				//IOWR(SEVEN_SEG_BASE,0 ,key);
				}
			else {
				flag = false;
			}
			}
			}
	}

}

void Button_Interrupt_ISR_Handler(void *pvParameters){
	while(1){
		if(xSemaphoreTake(signal_from_button_isr, portMAX_DELAY)){
			printf("Button ISR\n");
			 IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PUSH_BUTTON_BASE, 0x7);
			 if(!maintenanceModeOn){
				 maintenanceModeOn = true;

			 		printf("maintenanceModeOn\n");
			 	}else{
			 		 maintenanceModeOn = false;

			 		printf("maintenanceModeOFF\n");
			 	}


		}
	}

}
void ps2_isr (void* context, alt_u32 id)
{
	//Think of declaring variables as global, interrupt should not be long
	char ascii;
	int status = 0;
	unsigned char key = 0;
	KB_CODE_TYPE decode_mode;

	//Saving the context of the key immediately
	status = decode_scancode (context, &decode_mode , &key , &ascii);
	//Sending Data over xQueue

	xQueueSendFromISR(Global_Queue_Handle, &status, NULL);
	xQueueSendFromISR(Global_Queue_Handle, &ascii, NULL);
	xQueueSendFromISR(Global_Queue_Handle, &key, NULL);
	xQueueSendFromISR(Global_Queue_Handle, &decode_mode, NULL);

	xSemaphoreGiveFromISR(shared_resource_sem, NULL);

}
void vTimerCallback(xTimerHandle t_timer){ //Timer flashes green LEDs
	xSemaphoreGiveFromISR(signal_from_Timer_ISR,portMAX_DELAY);
}

void vTimerCallbackVGA(xTimerHandle t_timer){
	xSemaphoreGiveFromISR(signal_refresh_screen,portMAX_DELAY);
}
void taskSwitchesPolling(void *pvParameters)
{
	while(1){
		if(xSemaphoreTake(signal_from_Timer_ISR,portMAX_DELAY)){
			//printf("Polling Switches Task\n");
			int uiSwitchValue = 0;
		    // read the value of the switch and store to uiSwitchValue
			uiSwitchValue = IORD_ALTERA_AVALON_PIO_DATA(SLIDE_SWITCH_BASE); //the read value should be sent to the active loads on the screen
			// write the value of the switches to the red LEDs
			IOWR_ALTERA_AVALON_PIO_DATA(RED_LEDS_BASE, ((0x00FF)&uiSwitchValue)); //and the switches to display the correct leds
			// also write the value of the switches  to the seven segments display
			IOWR_ALTERA_AVALON_PIO_DATA(SEVEN_SEG_BASE, (0x00FF)&uiSwitchValue);

			int ones, tens;
			ones = ((0x000F)&uiSwitchValue);
			tens = ((0x00F0)&uiSwitchValue);

			//put this in critical section
			convertToBinary(tens,ones,&activeLoads);
			//convertToBinary(ones,&activeLoads); //this is creating two different copies of the same thing and that is why it doesnt work
			printf("%x\n",ones);
			printf("%x\n",tens);
			//convertToBinary(0x0000,&activeLoads);
			//convertToBinary(0,&activeLoads);
		}
	}
}


static void prvFirstRegTestTask(void *pvParameters)
{
	while (1)
	{
		printf("Task 1\n");
		vTaskDelay(1000);
	}

}

void getsem_task1(void *pvParameters)
{
	while (1)
	{
		xSemaphoreTake(shared_resource_sem, portMAX_DELAY);
	}

}

void Start_Interrupts(void *pvParameters){


	int buttonValue = 0;

	  // clears the edge capture register. Writing 1 to bit clears pending interrupt for corresponding button.
	  IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PUSH_BUTTON_BASE, 0x7);

	  // enable interrupts for all buttons
	  IOWR_ALTERA_AVALON_PIO_IRQ_MASK(PUSH_BUTTON_BASE, 0x7);

	  // register the ISR
	  alt_irq_register(PUSH_BUTTON_IRQ,(void*)&buttonValue, button_interrupt);
	  //vTaskDelete(Start_Interrupts);
}

void freq_relay(){
	#define SAMPLING_FREQ 16000.0
	double temp = SAMPLING_FREQ/(double)IORD(FREQUENCY_ANALYSER_BASE, 0);
	xQueueSendToBackFromISR(Q_freq_data_Monitor_Task,&temp,pdFALSE);
	xQueueSendToBackFromISR( Q_freq_data, &temp, pdFALSE );
	return;
}

void button_interrupt(void* context, alt_u32 id)
{
	 int* temp = (int*) context;
	  (*temp) = IORD_ALTERA_AVALON_PIO_EDGE_CAP(PUSH_BUTTON_BASE);
	  // clears the edge capture register
	  IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PUSH_BUTTON_BASE, 0x7);
	xSemaphoreGiveFromISR(signal_from_button_isr,NULL);

}
void PRVGADraw_Task(void *pvParameters ){
	if(xSemaphoreTake(signal_refresh_screen,portMAX_DELAY)){
	char frequencyBuffer[10]={0};
	char frequencyDataFromQueue[2]={0};
	//initialize VGA controllers
	alt_up_pixel_buffer_dma_dev *pixel_buf;
	pixel_buf = alt_up_pixel_buffer_dma_open_dev(VIDEO_PIXEL_BUFFER_DMA_NAME);
	if(pixel_buf == NULL){
		printf("can't find pixel buffer device\n");
	}
	alt_up_pixel_buffer_dma_clear_screen(pixel_buf, 0);

	alt_up_char_buffer_dev *char_buf;
	char_buf = alt_up_char_buffer_open_dev("/dev/video_character_buffer_with_dma");
	if(char_buf == NULL){
		printf("can't find char buffer device\n");
	}
	alt_up_char_buffer_clear(char_buf);



	//Set up plot axes											 /*THIS IS COLOR*/
	alt_up_pixel_buffer_dma_draw_hline(pixel_buf, 100, 590, 200, ((0x3ff << 20) + (0x3ff << 10) + (0x3ff)), 0);
	alt_up_pixel_buffer_dma_draw_hline(pixel_buf, 100, 590, 300, ((0x3ff << 20) + (0x3ff << 10) + (0x3ff)), 0);
	alt_up_pixel_buffer_dma_draw_vline(pixel_buf, 100, 50, 200, ((0x3ff << 20) + (0x3ff << 10) + (0x3ff)), 0);
	alt_up_pixel_buffer_dma_draw_vline(pixel_buf, 100, 220, 300, ((0x3ff << 20) + (0x3ff << 10) + (0x3ff)), 0);



	alt_up_char_buffer_string(char_buf, "Hi Arjun!", 2, 1);
	alt_up_char_buffer_string(char_buf, "System Active Time:", 55, 1);
	alt_up_char_buffer_string(char_buf, "Threshold Frequency(Hz):", 2, 40); //x,y pixel positions


	alt_up_char_buffer_string(char_buf, "Threshold ROC (Hz/s):", 50, 40); //x,y pixel positions
	alt_up_char_buffer_string(char_buf, "Loads Currently ON:", 2, 42);
	alt_up_char_buffer_string(char_buf, "Maintenance State Status:", 2, 44);
	alt_up_char_buffer_string(char_buf, "5 Immediate Readings:", 2, 46);
	alt_up_char_buffer_string(char_buf, "Max Time:", 50, 42);
	alt_up_char_buffer_string(char_buf, "Min Time:", 50, 44);
	alt_up_char_buffer_string(char_buf, "Condition Detected:", 2, 48);





	alt_up_pixel_buffer_dma_draw_hline(pixel_buf, 100, 1, 20, ((0x3ff << 20) + (0x3ff << 10) + (0x3ff)), 0);


	alt_up_char_buffer_string(char_buf, "Frequency(Hz)", 4, 4);
	alt_up_char_buffer_string(char_buf, "52", 10, 7); //x,y coordinates in the code
	alt_up_char_buffer_string(char_buf, "50", 10, 12);
	alt_up_char_buffer_string(char_buf, "48", 10, 17);
	alt_up_char_buffer_string(char_buf, "46", 10, 22);

	alt_up_char_buffer_string(char_buf, "df/dt(Hz/s)", 4, 26);
	alt_up_char_buffer_string(char_buf, "60", 10, 28);
	alt_up_char_buffer_string(char_buf, "30", 10, 30);
	alt_up_char_buffer_string(char_buf, "0", 10, 32);
	alt_up_char_buffer_string(char_buf, "-30", 9, 34);
	alt_up_char_buffer_string(char_buf, "-60", 9, 36);




	while(1){
		 if(maintenanceModeOn){
			alt_up_char_buffer_string(char_buf, "ON ", 28, 44);
			}else{

			alt_up_char_buffer_string(char_buf, "OFF", 28, 44);
			}


		 if(frequencyReadyToBeSend){


				//sprintf( frequencyBuffer,"%c",frequencyDataFromQueue ); //using xQueue

				sprintf(frequencyBuffer,"%c", inputBuffer[0]);
				alt_up_char_buffer_string(char_buf,frequencyBuffer, 32, 40);
				printf("xQueue received %d \n",frequencyBuffer[0]);
				indexCount = 0;
		 }else{
				alt_up_char_buffer_string(char_buf, "50", 30, 40);
				//printf("xQueue receive for new frequency failed\n");
			}


		 /*Display the active loads*/
		 if(activeLoads.L1==true){
				alt_up_char_buffer_string(char_buf, "L1", 22, 42);
		 }else{
			 alt_up_char_buffer_string(char_buf, "  ", 22, 42);
		 }

		 if(activeLoads.L2==true){
				alt_up_char_buffer_string(char_buf, "L2", 25, 42);
		 }else{
			 alt_up_char_buffer_string(char_buf, "  ", 25, 42);
		 }

		 if(activeLoads.L3==true){
				alt_up_char_buffer_string(char_buf, "L3", 28, 42);
		 }else{
			 alt_up_char_buffer_string(char_buf, "  ", 28, 42);
		 }

		 if(activeLoads.L4==true){
				alt_up_char_buffer_string(char_buf, "L4", 31, 42);
		 }else{
			 alt_up_char_buffer_string(char_buf, "  ", 31, 42);
		 }

		 if(activeLoads.L5==true){
				alt_up_char_buffer_string(char_buf, "L5", 34, 42);
		 }else{
			 alt_up_char_buffer_string(char_buf, "  ", 34, 42);
		 }


		 if(activeLoads.L6==true){
				alt_up_char_buffer_string(char_buf, "L6", 37, 42);
		 }else{
			 alt_up_char_buffer_string(char_buf, "  ", 37, 42);
		 }

		 if(activeLoads.L7==true){
				alt_up_char_buffer_string(char_buf, "L7", 40, 42);
		 }else{
			 alt_up_char_buffer_string(char_buf, "  ", 40, 42);
		 }

		 if(activeLoads.L8==true){
				alt_up_char_buffer_string(char_buf, "L8", 43, 42);
		 }else{
			 alt_up_char_buffer_string(char_buf, "  ", 43, 42);
		 }



		//receive frequency data from queue
		while(uxQueueMessagesWaiting( Q_freq_data ) != 0){
			xQueueReceive( Q_freq_data, freq+i, 0 );

			//calculate frequency RoC

			if(i==0){
				dfreq[0] = (freq[0]-freq[99]) * 2.0 * freq[0] * freq[99] / (freq[0]+freq[99]);
			}
			else{
				dfreq[i] = (freq[i]-freq[i-1]) * 2.0 * freq[i]* freq[i-1] / (freq[i]+freq[i-1]);
			}

			if (dfreq[i] > 100.0){
				dfreq[i] = 100.0;
			}


			i =	++i%100; //point to the next data (oldest) to be overwritten

		}

		//clear old graph to draw new graph
		alt_up_pixel_buffer_dma_draw_box(pixel_buf, 101, 0, 639, 199, 0, 0);
		alt_up_pixel_buffer_dma_draw_box(pixel_buf, 101, 201, 639, 299, 0, 0);

		for(j=0;j<99;++j){ //i here points to the oldest data, j loops through all the data to be drawn on VGA
			if (((int)(freq[(i+j)%100]) > MIN_FREQ) && ((int)(freq[(i+j+1)%100]) > MIN_FREQ)){
				//Calculate coordinates of the two data points to draw a line in between
				//Frequency plot
				line_freq.x1 = FREQPLT_ORI_X + FREQPLT_GRID_SIZE_X * j;
				line_freq.y1 = (int)(FREQPLT_ORI_Y - FREQPLT_FREQ_RES * (freq[(i+j)%100] - MIN_FREQ));

				line_freq.x2 = FREQPLT_ORI_X + FREQPLT_GRID_SIZE_X * (j + 1);
				line_freq.y2 = (int)(FREQPLT_ORI_Y - FREQPLT_FREQ_RES * (freq[(i+j+1)%100] - MIN_FREQ));

				//Frequency RoC plot
				line_roc.x1 = ROCPLT_ORI_X + ROCPLT_GRID_SIZE_X * j;
				line_roc.y1 = (int)(ROCPLT_ORI_Y - ROCPLT_ROC_RES * dfreq[(i+j)%100]);

				line_roc.x2 = ROCPLT_ORI_X + ROCPLT_GRID_SIZE_X * (j + 1);
				line_roc.y2 = (int)(ROCPLT_ORI_Y - ROCPLT_ROC_RES * dfreq[(i+j+1)%100]);

				//Draw
				alt_up_pixel_buffer_dma_draw_line(pixel_buf, line_freq.x1, line_freq.y1, line_freq.x2, line_freq.y2, 0x3ff << 0, 0);
				alt_up_pixel_buffer_dma_draw_line(pixel_buf, line_roc.x1, line_roc.y1, line_roc.x2, line_roc.y2, 0x3ff << 0, 0);
			}
		}
		//vTaskDelay(10);

	}

	}
}
int main()
{



							  // clears the edge capture register. Writing 1 to bit clears pending interrupt for corresponding button.
							  IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PUSH_BUTTON_BASE, 0x7);

							  // enable interrupts for all buttons
							  IOWR_ALTERA_AVALON_PIO_IRQ_MASK(PUSH_BUTTON_BASE, 0x1); //only using the right most button beofre the reset

							  // register the ISR
							  alt_irq_register(PUSH_BUTTON_IRQ,NULL, button_interrupt);


					/*PS2 ISR Declaration - START*/
					alt_up_ps2_dev * ps2_device = alt_up_ps2_open_dev(PS2_NAME);

					if(ps2_device == NULL){
						printf("can't find PS/2 device\n");
						return 1;
					}


					alt_up_ps2_clear_fifo (ps2_device) ;

					alt_irq_register(PS2_IRQ, ps2_device, ps2_isr);
					// register the PS/2 interrupt
					IOWR_8DIRECT(PS2_BASE,4,1);
					/*PS2 ISR Declaration - END*/


					/*Frequency Analyser ISR Declaration - START*/
					alt_irq_register(FREQUENCY_ANALYSER_IRQ, 0, freq_relay);
					/**Frequency Analyser ISR Declaration - END*/

					/*Push Button Interrupt*/

					/*REGISTER THE PUSH BUTTON ISR*/

					//mask the red leds interrupts



  	shared_resource_sem = xSemaphoreCreateBinary();
  	signal_from_button_isr = xSemaphoreCreateBinary();
  	signal_from_Timer_ISR = xSemaphoreCreateBinary();
  	signal_refresh_screen = xSemaphoreCreateBinary();

  	Global_Queue_Handle = xQueueCreate(4, sizeof(int));
  	Q_freq_data = xQueueCreate(100, sizeof(double));
  	Q_freq_data_Monitor_Task = xQueueCreate(100,sizeof(double)); //second queue to handle the data from the system
  	Queue_Task_Monitoring = xQueueCreate(1,sizeof(double));

  	Q_New_Frequency = xQueueCreate(7, sizeof(int)); //Example enterd value "100.00 enter key"


  	  //xTaskCreate(Start_Interrupts, "Task to start Interrupts",configMINIMAL_STACK_SIZE,NULL, StartInterrupt_Task_PRIORITY,NULL );
  	//xTaskCreate(getsem_task1		, "getsem_task1", TASK_STACKSIZE, NULL, GETSEM_TASK1_PRIORITY, NULL);



  		  xTaskCreate(taskMonitoring, "Monitor Loads", TASK_STACKSIZE, NULL, taskMonitoring_PRIORITY, NULL);
  	  	  xTaskCreate(Button_Interrupt_ISR_Handler,"Button ISR Handler", TASK_STACKSIZE, NULL, GET_BUTTON_ISR_HANDLER_PRIORITY, NULL);
  	  	  xTaskCreate(ps2_ISR_Handler		,"PS2 ISR Handler",TASK_STACKSIZE, NULL,GET_PS2_ISR_Handler_PRIORITY, NULL);
  		  xTaskCreate(taskSwitchesPolling, "Polling Switches",TASK_STACKSIZE, NULL,taskSwitchesPolling_PRIORITY ,NULL);
  		  //xTaskCreate( prvFirstRegTestTask, "Rreg1", configMINIMAL_STACK_SIZE, mainREG_TEST_1_PARAMETER, mainREG_TEST_PRIORITY, NULL);
  		  xTaskCreate( PRVGADraw_Task, "VGA Display"	, configMINIMAL_STACK_SIZE, NULL, PRVGADraw_Task_P, &PRVGADraw );

	  timer = xTimerCreate("Timer for Switches", 250, pdTRUE, NULL, vTimerCallback); //timers callback function is executed when timer has finished.
	  screenRefreshRate = xTimerCreate("Screen Refresh Rate timer",40,pdTRUE,NULL,vTimerCallbackVGA); //screen refresh rate set to monitor refresh rate
	  xTimerStart(timer, 0);
	  xTimerStart(screenRefreshRate, 0);
  			if (xTimerStart(timer, 0) != pdPASS){
  				printf("Cannot start timer");
  			}
  			/* Finally start the scheduler. */
	vTaskStartScheduler();


  while(1){}
  return 0;
}
