/*Save ISR context and then do the isr so need to create a task for
 * everything. ISR on itself will not work, IF MAKE FILE not working than usually problem with ISR*/
#include<system.h>
#include <altera_avalon_pio_regs.h>
#include <stdio.h>
/* Standard includes. */
#include <stddef.h>
#include <string.h>

/* Scheduler includes. */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/portmacro.h"


#include "io.h"
#include "altera_up_avalon_ps2.h"
#include "altera_up_ps2_keyboard.h"
#include "sys/alt_irq.h"

/* The parameters passed to the reg test tasks.  This is just done to check
 the parameter passing mechanism is working correctly. */
#define mainREG_TEST_1_PARAMETER    ( ( void * ) 0x12345678 )
#define mainREG_TEST_2_PARAMETER    ( ( void * ) 0x87654321 )
#define mainREG_TEST_PRIORITY       ( tskIDLE_PRIORITY + 1)

int flagWhenPrintfDoesntWork = 0;

void boss(void *p);
void employee(void *p);
void employee_task(void);

//Interrupt ISRs
void ps2_isr(void* p);
void button_interrupts_function(void *p);
//create actual tasks for the user inputs
void taskGetPS2UserInput(void *p);
void taskStartInterrupts(void *p);


void employee_task(){

}
//mutex is used when you want to limit access - one 1 person uses a guarded resource
//binary semaphore is slightly different, it is used as a signal between an interrupt and a task
//shared variables will require use of precious resource.
xQueueHandle Global_Queue_Handle = 0;
xQueueHandle keyboard_data_queue = 0;
//Minimum stack is about one third

xSemaphoreHandle employee_signal = 0;
SemaphoreHandle_t getUserPS2input_signal = 0;
//semaphore gurantees us that only one task will be going through the statement
void boss(void *p)
{
	while(1){
		printf("Boss about giving the signal\n");
		xSemaphoreGive(employee_signal);//freeRTOS realizes that someone was waiting for the signal and employee task had the higher priority and therefore swithces right away
		printf("Boss giving the signal\n");
		vTaskDelay(2000);//when this task goes to sleep only then employee task wakes up
		//once we are done with the gate keeper we need to tell them that we are done
	}

}


void employee(void *p)
{
	while(1){
		//when we take xsemaphore we give it back in this case the boss is giving the signal and the employee is taking the signal
		//employee is waiting and not consuming any cpu
		//employee task will sleep because of the portMAX_DELAY
			if(xSemaphoreTake(employee_signal,portMAX_DELAY)){ //how long we are willing to wait for the xsemaphore
				employee_task();

				printf("Employee has finished its task \n");
				}
			}
}


void taskStartInterrupts (void *p){

		/*Start the BUTTON ISR*/
			int buttonValue = 1;
			void* button_isr_context = (void*)&buttonValue;
			IOWR_ALTERA_AVALON_PIO_EDGE_CAP(PUSH_BUTTON_BASE,0);
			IOWR_ALTERA_AVALON_PIO_IRQ_MASK(PUSH_BUTTON_BASE,0x7);
			IOWR_ALTERA_AVALON_PIO_DATA(GREEN_LEDS_BASE,0x01);
			//alt_irq_register(PUSH_BUTTON_IRQ,button_isr_context, button_interrupts_function);

			/*Start the Keyboard PS2 ISR*/
			alt_up_ps2_dev * ps2_device = alt_up_ps2_open_dev(PS2_NAME);
			if(ps2_device == NULL){
				printf("Can't find PS/2 device\n");

			}
			alt_up_ps2_clear_fifo (ps2_device) ;
			alt_irq_register(PS2_IRQ, ps2_device, ps2_isr);
			void* context_going_to_be_passed=(void*)&ps2_device;

			//first argument irq number of the interrupt, second argument is the pointer to the information that we pass to the isr, third pointer is the function pointer
			//alt_irq_register(PS2_IRQ, ps2_device, ps2_isr);
			// register the PS/2 interrupt
			IOWR_8DIRECT(PS2_BASE,4,1);

			//printf("%x \n",flagWhenPrintfDoesntWork);
			printf("Task Start Interrupts\n");
			if(!xQueueSend(keyboard_data_queue, &context_going_to_be_passed, 1000)){
				printf("Failed to send to queue\n");
			}else{
				printf("Data Sent Successfully on xQueue \n");
			}
			vTaskDelay(1000);

			vTaskDelete(taskStartInterrupts); //since task only executes once
}

/*Keyboard ISR handler*/
void taskGetPS2UserInput(void *p)
{


	while(1){
		if(xSemaphoreTakeFromISR(getUserPS2input_signal,portMAX_DELAY)){
			printf("Semaphore received successfully\n");

			int context;
			if(xQueueReceive(keyboard_data_queue,&context,1000)){
				printf("Data Received Successfully\n");
			}else{
				printf("Failed to receive data\n");
			}

			printf("Task Keyboard ISR Handler\n");

			/*Task waiting for semaphore to do work*/


			//printf("%x ",flagWhenPrintfDoesntWork);
			char ascii;
			int status = 0;
			unsigned char key = 0;
			KB_CODE_TYPE decode_mode;

			status = decode_scancode (context, &decode_mode , &key , &ascii) ;
			if ( status == 0 ) //success
			{
			// print out the result
				switch ( decode_mode )
				{
					case KB_ASCII_MAKE_CODE :
						printf ( "ASCII   : %x\n", key ) ;
						break ;
					case KB_LONG_BINARY_MAKE_CODE :
					// do nothing
					case KB_BINARY_MAKE_CODE :
						printf ( "MAKE CODE : %x\n", key ) ;
						break ;
					case KB_BREAK_CODE :
					// do nothing
					default :
						printf ( "DEFAULT   : %x\n", key ) ;
						break ;
				}
			IOWR(SEVEN_SEG_BASE,0 ,key);
			}
		}
	}
}

/*Interrupt PS2_ISR - Send the input over*/
void ps2_isr(void* p)
{
	//xSemaphoreGiveFromISR( getUserPS2input_signal, NULL );
	//signalling semaphore to follow the theory of synchrony. everything happens right away no time delay
	/*WARNING NO PRINTF IN ISR*/
	//uxSavedInterruptsStatus =  taskENTER_CRITICAL_FROM_ISR();
	//taskEXIT_CRITICAL_FROM_ISR(uxSavedInterruptsStatus);

		 //if(task_woken){
		//	 port_YIELD_FROM_ISR(); --> goes to the task of higher priorty that was waiting for this task
		 //}
}



void button_interrupts_function(void *p){
	//xSemaphoreGiveFromISR( getUserPS2input_signal, NULL );

}
int main(void){
	//enableFlushAfterPrintf();
	//we create a queue before we create a task
	//gatekeeper = xSemaphoreCreateMutex();

	/*Name binary semaphores binary*/
	vSemaphoreCreateBinary(employee_signal); //v means void doesn't return anything
	vSemaphoreCreateBinary(getUserPS2input_signal);


	Global_Queue_Handle = xQueueCreate(3, sizeof(int));
	keyboard_data_queue = xQueueCreate(2, sizeof(int));

	/*Create Tasks*/
	//xTaskCreate(boss, (signed char*) "t1",1024, NULL,1,NULL);
	//xTaskCreate(employee, (signed char*) "t2",1024, NULL,9,NULL);

	xTaskCreate(taskGetPS2UserInput, (signed char*)"UserInput_Task"  ,1024, NULL,6,NULL);
	xTaskCreate(taskStartInterrupts, (signed char*)"Start_Interrupts",1024, NULL,7,NULL);
	//xTaskCreate(ps2_isr, (signed char*)"ISR_Task",1024,NULL,5,NULL);
	vTaskStartScheduler();
	return 0;

}
